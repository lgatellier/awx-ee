# AWX Execution Environment

Basic AWX EE with following dependencies :
- Python [`hvac`][1] library
- Ansible [`community.general`][2] collection
- Ansible [`community.hashi_vault`][3] collection

[1]: https://github.com/hvac/hvac
[2]: https://docs.ansible.com/ansible/latest/collections/community/general/index.html
[3]: https://docs.ansible.com/ansible/latest/collections/community/hashi_vault/index.html